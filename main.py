from fastapi import FastAPI
from api import router as api_router
from db import Base,engine

app = FastAPI()
app.include_router(api_router)
base= Base.metadata.create_all(bind=engine)

if __name__ == "__main__":
    import uvicorn
    
    uvicorn.run(app, host="0.0.0.0", port=8000)