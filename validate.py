from pydantic import BaseModel
from datetime import datetime
from typing import Optional
class AuthorCreate(BaseModel):
    name: str
    biography: str

class AuthorResponse(BaseModel):
    id: int
    name: str
    biography: str

class BookCreate(BaseModel):
    title: str
    author_id: int
    genre: str
    published_date: datetime
    isbn: str
    quantity_available: int

class BookResponse(BaseModel):
    id: int
    title: str
    author_id: int
    genre: str
    published_date: datetime
    isbn: str
    quantity_available: int

class BookUpdate(BaseModel):
    title: Optional[str]
    author_id: Optional[int]
    genre: Optional[str]
    published_date: Optional[str]
    isbn: Optional[str]
    quantity_available: Optional[int]