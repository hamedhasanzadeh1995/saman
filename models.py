from sqlalchemy import Boolean, Column, ForeignKey, Integer, String , DateTime  , Text
from sqlalchemy.orm import relationship
from db import Base


class Author(Base):
    __tablename__ = 'authors'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255), nullable=False)
    biography = Column(Text,nullable=True)

    
    books = relationship('Book', back_populates='author')

class Book(Base):
    __tablename__ = 'books'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255), nullable=False)
    author_id = Column(Integer, ForeignKey('authors.id'), nullable=False)
    genre = Column(String(50))
    published_date = Column(DateTime)
    isbn = Column(String(20))
    quantity_available = Column(Integer, default=0)

   
    author = relationship('Author', back_populates='books')

