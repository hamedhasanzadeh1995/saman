from fastapi import APIRouter, HTTPException,Depends , Query
from sqlalchemy import  orm
from models import Author,Book
from validate import BookCreate,BookResponse ,BookUpdate,AuthorCreate ,AuthorResponse
from db import get_db

router = APIRouter()
@router.get("/books", response_model=list[BookResponse])
def get_all_books(
    genre: str = Query(None, title="Filter by Genre"),
    author_id: int = Query(None, title="Filter by Author ID"),
    sort_by: str = Query(None, title="Sort by Published Date (asc or desc)"),
    db: orm.Session = Depends(get_db)
    ):
    filters = {}
    if genre:
        filters["genre"] = genre
    if author_id is not None:
        filters["author_id"] = author_id
    query = db.query(Book).filter_by(**filters)

    #  sorting 
    if sort_by:
        if sort_by.lower() == "asc":
            query = query.order_by(Book.published_date.asc())
        elif sort_by.lower() == "desc":
            query = query.order_by(Book.published_date.desc())
        else:
            raise HTTPException(status_code=400, detail="Invalid value for 'sort_by'. Use 'asc' or 'desc'.")

    
    books = query.all()
    return books

@router.get("/books/{book_id}", response_model=BookResponse)
def get_book(book_id: int, db: orm.Session = Depends(get_db)):
    book = db.query(Book).filter(Book.id == book_id).first()
    if book is None:
        raise HTTPException(status_code=404, detail="book not found")
    return book

@router.post("/books", response_model=BookResponse)
def create_book(book: BookCreate, db: orm.Session = Depends(get_db)):
    author = db.query(Author).filter(Author.id == book.author_id).first()
    if author is None:
        raise HTTPException(status_code=400, detail="book not create")
    
    try:
        db_book = Book(**book.model_dump())
        db.add(db_book)
        db.commit()
        db.refresh(db_book)
        return db_book
    except Exception as e:
        raise HTTPException(status_code=422, detail=str(e))

@router.put("/books/{book_id}", response_model=BookResponse)
def update_book(book_id: int, updated_book: BookUpdate, db: orm.Session = Depends(get_db)):
    book = db.query(Book).filter(Book.id == book_id).first()
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    for field, value in updated_book.model_dump().items():
        setattr(book, field, value)
    db.commit()
    db.refresh(book)
    return book

@router.delete("/books/{book_id}", response_model=dict)
def delete_book(book_id: int, db: orm.Session = Depends(get_db)):
    book = db.query(Book).filter(Book.id == book_id).first()
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    db.delete(book)
    db.commit()
    return {"message": "Book deleted successfully"}



@router.get("/authors", response_model=list[AuthorResponse])
def get_all_authors(db: orm.Session = Depends(get_db)):
    authors = db.query(Author).all()
    return authors

@router.get("/authors/{author_id}", response_model=AuthorResponse)
def get_author(author_id: int, db: orm.Session = Depends(get_db)):
    author = db.query(Author).filter(Author.id == author_id).first()
    if author is None:
        raise HTTPException(status_code=404, detail="Author not found")
    return author

@router.post("/authors", response_model=AuthorResponse)
def create_author(author: AuthorCreate, db: orm.Session = Depends(get_db)):
    db_author = Author(**author.model_dump())
    db.add(db_author)
    db.commit()
    db.refresh(db_author)
    return db_author
